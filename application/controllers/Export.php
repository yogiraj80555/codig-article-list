<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class export extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Export_model','export');
	}

	public function index()
	{
		$data['title'] = "Display Feedback Data";
		$data['feedbackinfo'] = $this->export->feedback_list();
		$this->load->view('users/feedbacklist',$data);
	}


	public function createXLS(){
		$this->load->library("excel");
		$phpexcel = new PHPExcel();
		$phpexcel->setActiveSheetIndex(0);
		$table_colums = array("sr","Name","Email","Feedback");
		foreach($table_colums as $key=>$field){
			$phpexcel->getActiveSheet()->setCellValueByColumnAndRow($key,1, $field);
		}
		$feedbackinfo = $this->export->feedback_list();
		foreach($feedbackinfo as $key=>$field){
			$phpexcel->getActiveSheet()->setCellValueByColumnAndRow(0,$key+2,$key+1);
			$phpexcel->getActiveSheet()->setCellValueByColumnAndRow(1,$key+2,$field->name);
			$phpexcel->getActiveSheet()->setCellValueByColumnAndRow(2,$key+2,$field->email);
			$phpexcel->getActiveSheet()->setCellValueByColumnAndRow(3,$key+2,$field->feedback);
		}
		$object_writer = PHPExcel_IOFactory::createWriter($phpexcel,'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="FeedbackData.xls"');
		$object_writer->save('php://output');

	}
}
?>