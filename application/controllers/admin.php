	<?php
	class Admin extends CI_Controller{

		public function __construct()
		{
			parent::__construct();

		}
		public function index(){$this->login();}
		
		private function login()
		{
			//echo "Testing Admin";
			if($this->session->userdata('id'))
			{
				return redirect('admin/welcome'); 
			}

			$this->load->library('form_validation');
			$this->form_validation->set_rules('username','Email ','required');//|valid_email
			$this->form_validation->set_rules('password','Password','required|max_length[12]');
			$this->form_validation->set_error_delimiters("<div class='text-danger'>","</div>");
			if($this->form_validation->run()){
				$this->load->model('loginmodel');
				$details = $this->loginmodel->isvalidate($this->input->post('username'),$this->input->post('password'));
				if($details['id']){
					$this->load->library('session');
					$this->session->set_userdata('id',$details['id']);
					$this->session->set_userdata('username',$details['name']);
					return redirect('admin/welcome');
				}else{

					$this->session->set_flashdata('Login_failed','Invalid Username Password');
					$this->load->view('Admin/login');}
				}else{
					$this->load->view('Admin/login');}
				}


				public function logout()
				{
					session_destroy();
					return redirect('admin/welcome');
				}

				public function welcome()
				{
					if(!$this->session->userdata('id')){
						return redirect('admin/index');
					}
					$this->load->library('form_validation');
					$this->load->library('session');
					$this->load->model('loginmodel','artis');
			#implement pagination
					$config=[
						'base_url'=>base_url('admin/welcome'),
						'per_page'=>2,
						'total_rows'=>$this->artis->numrows(),
						'full_tag_open'=>'<ul class="pagination pagination-sm">',
						'full_tag_close'=>'</ul>',
						'next_tag_open'=>'<li class="page-item">',      
						'next_tag_close'=>'</li>',
						'prev_tag_open'=>'<li class="page-item">',
						'prev_tag_close'=>'</li>',
						'num_tag_open' => '<li>',
						'num_tag_close'=>'</li>',
						'cur_tag_open'=>'<li class="page-item active"><a class="page-link">',
						'cur_tag_close'=>'</a></li>'
					];
					$this->load->library('pagination');
					$this->pagination->initialize($config);

					$articles = $this->artis->articleList($config['per_page'],$this->uri->segment(3)); 
					$this->load->view('admin/dashboard',['articles'=>$articles]);
				}

				public function register()
				{
					$this->load->library('form_validation');
					$this->form_validation->set_rules('uname','Username/Email','required|valid_email',array('required'=>'You have not provided %s.'));
					$this->form_validation->set_rules('upass','Password','required|max_length[12]');
					$this->form_validation->set_rules('fname','First Name','required');
					$this->form_validation->set_rules('lname','Last Name','required');
					$this->form_validation->set_error_delimiters("<div class='text-danger'>","</div>");
					if($this->form_validation->run()){
						$this->load->library('email');
						$this->email->from(set_value('uname'),set_value('fname'));
						$this->email->to("info@yogiraj.ml");
						$this->email->subject("Greetings for Registration");

						$this->email->message("Thankyou For Registration");
						$this->email->set_newline("\r\n");
				//$this->email->send();

				/*if($this->email->send()){
					
				}else{
					//show_error($this->email->print_debugger());
				}*/
				$this->load->model('loginmodel','register');
				if($this->register->register_user($this->input->post()))
				{
					$this->session->set_flashdata('register','Registration Successfully');
					return redirect('admin/index');
				}else
				{
					$this->session->set_flashdata('register','There is some issue');
					$this->load->view('admin/register_user');


				}


			}else
			{
				$this->load->view('admin/register_user');
			}
		}


		public function addarticle()
		{
			if(!$this->session->userdata('id')){
				return redirect('admin/index');
			}
			$this->load->library('form_validation');
			$this->load->view('admin/add_article');
		}


		public function article_validation()
		{
			$this->load->library('form_validation');
			if($this->form_validation->run('add_article_rule'))
			{
				$this->load->model('loginmodel','addarticle');
				if($this->addarticle->add_article($this->input->post()))
				{
					$this->session->set_flashdata('article_adding','Article Added Successfully');
					$this->session->set_flashdata('article_adding_class','alert-success');
					return redirect('admin/welcome');
				}
				else{
					$this->session->set_flashdata('article_adding','There is someproblem occers');
					$this->session->set_flashdata('article_adding_class','alert-danger');
					return redirect('admin/welcome');
				}
			}
			else{
				$this->load->view('admin/add_article');
			}
		}

		public function delarticle()
		{
			if(!$this->session->userdata('id')){
				return redirect('admin/index');
			}
			
			$this->load->model('loginmodel','deletearticleid');
			if($this->deletearticleid->deletearticle($this->input->post()))
			{
				$this->session->set_flashdata('article_adding','Article Deleted Successfully');
				$this->session->set_flashdata('article_adding_class','alert-success');
				return redirect('admin/welcome');
			}
			else{
				$this->session->set_flashdata('article_adding','Sorry There is some problem');
				$this->session->set_flashdata('article_adding_class','alert-danger');
				return redirect('admin/welcome');	
			}
		}

		public function editarticle()
		{
			if(!$this->session->userdata('id')){
				return redirect('admin/index');
			}
			$this->load->library('form_validation');
			$this->load->model('loginmodel','editart');
			$this->load->view('admin/edit_article',['data'=>$this->editart->edit_article($this->input->get('id'))]);
		}

		public function updatearticle()
		{
			if(!$this->session->userdata('id')){
				return redirect('admin/index');
			}
			$this->load->library('form_validation');
			if($this->form_validation->run('add_article_rule')){
				$this->load->model('loginmodel','updatearticle');
				if($this->updatearticle->update_article($this->input->post()))
				{	
					$this->session->set_flashdata('article_adding','Article Updated successfully');
					$this->session->set_flashdata('article_adding_class','alert-dismissible alert-success');
				}else{
					$this->session->set_flashdata('article_adding','Sorry! Article could not updated');
					$this->session->set_flashdata('article_adding_class','alert-dismissible alert-danger');
				}
				return redirect('admin/welcome');
			}else{
				$this->load->model('loginmodel','editart');
				$this->load->view('admin/edit_article',['data'=>$this->editart->edit_article($this->input->post('id'))]);
			}
		}
	}

	?>