<?php include('header.php') ?>
<div class='container' style="margin-top:20px;">
	<h1><?= $title; ?></h1>
	<div class="table-responsive" style="margin-top:20px;">
		<table class="table table-hover tablesorter">
			<thead>
				<tr>
					<th class="header">S.No</th>
					<th class="header">Name</th>
					<th class="header">Email</th>
					<th class="header">Feedback</th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach($feedbackinfo as $key=>$row):?>
						<tr>
							<td><?= $key+1 ?></td>
							<td><?= $row->name ?></td>
							<td><?= $row->email ?></td>
							<td><?= $row->feedback ?></td>
						</tr>
					<?php endforeach;
				?>
			</tbody>
		</table>
		<div align="center">
			<form method="post" action="<?= base_url(); ?>export/createXLS">
				<input type="submit" name="export" class="btn btn-outline-info" value="Export" />
			</form>
		</div>
	</div>
</div>
<?php include('footer.php') ?>