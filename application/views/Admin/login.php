<?php include('header.php') ?>
<div class="container" style="margin-top: 30px;">
	<h1> Login Form </h1><br>
	<?php if($error = $this->session->flashdata('Login_failed')): ?>
		<div class="row">
			<div class="col-lg-4">
				<div class="alert alert-danger">
					<?php echo $error; ?>
				</div>
			</div>
		</div>

	<?php endif; ?>

	<?php if($error = $this->session->flashdata('register')): ?>
		<div class="row">
			<div class="col-lg-4">
				<div class="alert alert-success">
					<?php echo $error; ?>
				</div>
			</div>
		</div>

	<?php endif; ?>
	<br>
	<?php  echo form_open('admin/index'); ?>

	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<lable for="email">Email Address:</lable>
				<?php echo form_input(['class'=>'form-control','placeholder'=>' Email','name'=>'username','value'=>set_value('username')]); ?>
			</div>
		</div>
		<div class="col-lg-6" style="margin-top: 30px">
			<?php echo form_error('username') ?>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<lable for="pwd">Password:</lable>
				<?php echo form_password(['class'=>'form-control','placeholder'=>' Password','name'=>'password','value'=>set_value("password")]); ?>
				<!--<input type="password" class="form-control" id="pwd"> -->
			</div>
		</div>
		<div class="col-lg-6" style="margin-top: 30px">
			<?php echo form_error('password') ?>
		</div>
	</div>



	<?php echo form_submit(['class'=>'btn btn-primary','value'=>'Login']); ?>
	<?php echo form_reset(['class'=>'btn btn-outline-secondary','value'=>'Reset']); ?>
	<?php echo anchor('admin/register/','Sign up?','class="link-class"'); ?>
</form>
</div>

<?php include('footer.php') ?>