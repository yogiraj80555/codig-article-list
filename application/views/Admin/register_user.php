<?php include('header.php')?>

<div class="container" style="margin-top:25px;">
	<h1>Register Form</h1>


	<?php if($error = $this->session->flashdata('register')): ?>
		<div class="row">
			<div class="col-lg-4">
				<div class="alert alert-success">
					<?php echo $error; ?>
				</div>
			</div>
		</div>

	<?php endif; ?>
	

<?php  echo form_open('admin/register'); ?>

	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label for="Username">Email:</label>
				<?php echo form_input(['class'=>'form-control','placeholder'=>'Enter Email','name'=>'uname','value'=>set_value('uname')]); ?>
			</div>
		</div>
		<div class="col-lg-6" style="margin-top: 30px">
		<?php echo form_error('uname') ?>
	</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label for="password">Password:</label>
				<?php echo form_input(['class'=>'form-control','placeholder'=>'Enter password','name'=>'upass','value'=>set_value('upass')]); ?>
			</div>
		</div>
		<div class='col-lg-6' style="margin-top:30px">
		<?php echo form_error('upass'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label for="name">First Name:</label>
				<?php echo form_input(['class'=>'form-control','placeholder'=>'Enter FirstName','name'=>'fname','value'=>set_value('fname')]); ?>
			</div>
		</div>
		<div class='col-lg-6' style="margin-top:30px">
		<?php echo form_error('fname'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label for="name">Last Name:</label>
				<?php echo form_input(['class'=>'form-control','placeholder'=>'Enter LastName','name'=>'lname','value'=>set_value('lname')]); ?>
			</div>
		</div>
		<div class='col-lg-6' style="margin-top:30px">
		<?php echo form_error('lname'); ?>
		</div>
	</div>
			<?php echo form_submit(['type'=>'submit','class'=>'btn btn-primary','value'=>'Register']);
			echo form_reset(['type'=>'reset','class'=>'btn btn-outline-secondary','value'=>'Reset']);
			?>
</form>
	</div>

<?php include('footer.php') ?>