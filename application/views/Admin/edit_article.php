<?php include('header.php') ?>
<div class="container" style="margin-top: 30px;">
	<h1> Edit Article </h1><br>

	<?php  echo form_open('admin/updatearticle'); ?>
	<?php echo form_hidden('id',$data->id ); ?>
	
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<lable for="Title"> Article Title:</lable>
				<?php echo form_input(['class'=>'form-control','placeholder'=>'Enter Title','name'=>'title','style'=>'margin-top:6px;','value'=>set_value('title',$data->article_title)]); ?>
			</div>
		</div>
		<div class="col-lg-6" style="margin-top: 30px">
			<?php echo form_error('title') ?>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<lable for="body">Article Body:</lable>
				<?php echo form_textarea(['class'=>'form-control','placeholder'=>' Enter Body','style'=>'margin-top:6px;','name'=>'body','value'=>set_value("body",$data->article_body)]); ?>
				<!--<input type="password" class="form-control" id="pwd"> -->
			</div>
		</div>
		<div class="col-lg-6" style="margin-top: 30px">
			<?php echo form_error('body') ?>
		</div>
	</div>



	<?php echo form_submit(['class'=>'btn btn-primary','value'=>'Submit']); ?>
	<?php echo form_reset(['class'=>'btn btn-outline-secondary','value'=>'Reset']); ?>
	
</form>
</div>

<?php include('footer.php') ?>