<?php include('header.php');?>

<?php if($error = $this->session->flashdata('article_adding')): ?>

	<div class="container" style="margin-top:10px">
		<div class="row">
			<div class="col-lg-4">
				<div class="alert <?= $this->session->flashdata('article_adding_class') ?>">
					<?php echo $error; ?>
				</div>
			</div>
		</div>
	</div>

<?php endif; ?>



<div class='container' style="margin-top: 30px">
	<div class="row">
		<a href="<?= base_url("/admin/addarticle")?>" class="btn btn-sm btn-primary">Add Article</a>
	</div>
</div>


<div class='container ' style="margin-top: 40px;">
	<div class='table'>
		<table>
			<thead>
				<tr>
					<th>ID</th>
					<th>Article Title</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
				<?php if(count($articles)): ?>
					<?php foreach ($articles as $key=>$arts): ?>
						<tr>
							<td><?php echo $key+1; ?></td>
							<td><?php echo $arts->article_title; ?></td>
							<td><a href="<?= base_url("/admin/editarticle?&id=$arts->id")?> " class="btn btn-primary">Edit</a></td>
							<td><?= 
								form_open('admin/delarticle'),
								form_hidden('id',$arts->id),
								form_submit(['type'=>'submit','value'=>'Delete','class'=>'btn btn-danger']),
								form_close();

							 ?>



								
						</tr>
					<?php endforeach;?>
					<?php else: ?>
						<tr>
							<td colspan="4">No Data Avalible</td>
						</tr>
					<?php endif; ?>
				</tbody>
				
			</table>

<!-- 

<div>
  <ul class="pagination pagination-sm">
    <li class="page-item disabled">
      <a class="page-link" href="#">&laquo;</a>
    </li>
    <li class="page-item active">
      <a class="page-link" href="#">1</a>
    </li>
    <li class="page-item">
      <a class="page-link" href="#">2</a>
    </li>
    <li class="page-item">
      <a class="page-link" href="#">3</a>
    </li>
    <li class="page-item">
      <a class="page-link" href="#">4</a>
    </li>
    <li class="page-item">
      <a class="page-link" href="#">5</a>
    </li>
    <li class="page-item">
      <a class="page-link" href="#">&raquo;</a>
    </li>
  </ul>
</div>

-->



			<?php echo $this->pagination->create_links(); ?> 
		</div>
	</div>
	<?php include('footer.php'); ?>