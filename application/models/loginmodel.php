<?php
	class loginmodel extends CI_Model{

		private function load_database()
		{
			return  $this->load->database();
		}

		public function isvalidate($username,$password)
		{
			$this->load_database();
			$q = $this->db->where(['username'=>$username,'password'=>$password])->get("user");

			if($q->num_rows())
			{	
				$data['id'] = $q->row()->id;
				$data['name'] = $q->row()->firstname." ".$q->row()->lastname;
				return $data;
			}else
			{
				return False;
			}
		}

		public function articleList($limit,$offset){
			$this->load->database();
			$this->load->library('session');
			$id = $this->session->userdata('id');

			return $this->db->select(['article_title','article_body','id'])->limit($limit,$offset)->where(['user_id'=>$id])->get('articles')->result(); 
		}

		public function register_user($post){
			$this->load_database();
			$array = array('username' => $post['uname'],
				'password' => $post['upass'],
				'firstname' => $post['fname'],
				'lastname' => $post['lname']);
			return $this->db->insert('user',$array);
		}

		public function add_article($post){
			$this->load_database();
			$array = array('article_title'=>$post['title'],
				'article_body'=>$post['body'],
				'user_id'=>$post['user_id']);
			$affected_rows = $this->db->insert('articles',$array);
			return $affected_rows;
		}

		public function deletearticle($id){
			$this->load_database();
			$affected_rows = $this->db->delete('articles',$id);
			return $affected_rows;
		}

		public function numrows(){
			$this->load->database();
			$this->load->library('session');
			$id = $this->session->userdata('id');
			return $this->db->select('id')->where(['user_id'=>$id])->get('articles')->num_rows();
		}

		public function edit_article($id){
			$this->load_database();
			return $this->db->select(['article_title','article_body','id'])->where('id',$id)->get('articles')->row(); 
		}

		public function update_article($data){
			$this->load_database();
			$array = array('article_title'=>$data['title'],'article_body'=>$data['body']);
			$affected_row = $this->db->update('articles',$array,['id'=>$data['id']]);
			return $affected_row;
		}

	}

	?>