<?php

$config =[
		'add_article_rule'=>[
			[
				'field' => 'title',
				'label' => 'Article Title',
				'rules' => 'required'
			],
			[
				'field' => 'body',
				'label' => 'Article Body',
				'rules' => 'required'
			]
		],
]

?>